import tables, sequtils, strutils, macros, types

#Utils

type
    LogicRef = distinct uint
    PinRef = distinct uint
    NetRef = distinct uint

proc intSearchAndReplace(b: NimNode, ocultVar: NimNode): NimNode =
    case b.kind:
        of nnkNone, nnkEmpty, nnkNilLit, nnkCharLit..nnkUInt64Lit, nnkFloatLit..nnkFloat64Lit, nnkStrLit..nnkTripleStrLit, nnkCommentStmt: 
            result = b
        of nnkIdent, nnkSym:
            if b.strVal.substr(1).isDigit and (b.strVal[0] == 'P' or b.strVal[0] == 'L' or b.strVal[0] == 'N'):
                var val = b.strVal.substr(1).parseInt.newIntLitNode
                var kind = if b.strVal[0] == 'P':
                        "PinRef"
                    elif b.strVal[0] == 'L':
                        "LogicRef"
                    else:
                        "NetRef"

                result = nnkStmtList.newTree(nnkDotExpr.newTree(nnkDotExpr.newTree(val,newIdentNode("uint")),newIdentNode(kind)))
            elif b.strVal.substr(2).isDigit and b.strVal[0] == 'C':
                var s = b.strVal.substr(1, 1).newStrLitNode
                var e = b.strVal.substr(2).parseInt.newIntLitNode
                result = nnkPar.newTree(
                    nnkExprColonExpr.newTree(newIdentNode("r"),s),
                    nnkExprColonExpr.newTree(newIdentNode("n"),nnkDotExpr.newTree(e,newIdentNode("uint")))
                    )
            else:
                result = b
        of nnkCall:
            let val = b[0].strVal
            if val == "pins" or val == "pinsLogic" or val == "nets" or val == "buses" or val == "components" or val == "connections":
                result = newCall(val, ocultVar, intSearchAndReplace(b[1], ocultVar))
            else:
                result = copyNimNode(b)
                for child in b.children:
                    result = result.add intSearchAndReplace(child, ocultVar)
        of nnkDotExpr:
            if b[0].kind == nnkDotExpr and b[0][0].kind == nnkIdent and b[0][0].strVal.substr(1).isDigit and b[0][0].strVal[0] == 'P':
                var comp = intSearchAndReplace(b[0][1], ocultVar)
                var p1 = nnkDotExpr.newTree(intSearchAndReplace(b[1], ocultVar),newIdentNode("uint"))
                var p2 = nnkDotExpr.newTree(intSearchAndReplace(b[0][0], ocultVar),newIdentNode("uint"))

                result = nnkBracket.newTree(
                    nnkPar.newTree(nnkExprColonExpr.newTree(newIdentNode("c"),comp),nnkExprColonExpr.newTree(newIdentNode("p"),p2)),
                    nnkPar.newTree(nnkExprColonExpr.newTree(newIdentNode("c"),comp),nnkExprColonExpr.newTree(newIdentNode("p"),p1)))
            elif b[0].kind == nnkIdent and b[0].strVal.substr(2).isDigit and b[0].strVal[0] == 'C':
                discard
            else:
                result = copyNimNode(b)
                for child in b.children:
                    result = result.add intSearchAndReplace(child, ocultVar)
        else:
            result = copyNimNode(b)
            for child in b.children:
                result = result.add intSearchAndReplace(child, ocultVar)

macro searchAndReplace(b: untyped, ocultVar: untyped = ""): untyped =
    return intSearchAndReplace(b, ocultVar)

#[macro createCompFunc(name: string, body: untyped): untyped =
    var empty = newEmptyNode()
    result = buildMacro:
        procDef:
            postfix(ident("*"), ident(name))
            empty
            empty
            formalParams:
                ident("Component")
            empty
            empty
            stmtList:
                body
                empty

macro createCircVar(name: string): NimNode =
    var empty = newEmptyNode()
    result = buildMacro:
        varSection:
            identDefs(ident(name), ident("Circuit"), empty)

macro assignCircVar(name: string, val: NimNode): NimNode =
    result = buildMacro:
        asgn(ident(name), val)]#

#Pins 

template PinIn*(volt: tuple[min: float, max: float], amp: tuple[min: float, max: float]): Pin =
    Pin(minVoltage: volt[0], maxVoltage: volt[1], minCurrent: amp[0], maxCurrent: amp[1], kind: Input)

template PinOut*(volt: tuple[min: float, max: float] = (0.0,0.0), amp: tuple[min: float, max: float] = (0.0,0.0)): Pin =
    Pin(minVoltage: volt[0], maxVoltage: volt[1], minCurrent: amp[0], maxCurrent: amp[1], needforward: true, kind: Output)    

template PinIO*(volt: tuple[min: float, max: float], amp: tuple[min: float, max: float]): Pin =
    Pin(minVoltage: volt.min, maxVoltage: volt.max, minCurrent: amp.min, maxCurrent: amp.max, kind: Io)

#[template Net(): Net =
    #TODO: better??
    Net(pins: @[])]#

#Definition

template cirucit*(name: string, body: untyped): Circuit =
    var final = Circuit(components: initTable[string, Table[uint, Component]](), buses: @[], nets: @[], ignore: @[])
    searchAndReplace(body, final)
    final

template component*(compName: string, body: untyped): Component =
    var final = Component(name: compName, pins: initTable[uint, tuple[pin: Pin, conns: seq[uint]]](), logics: initTable[uint, proc(netIn: var Net, p1, p2: var tuple[n: uint, p: Pin])]())
    searchAndReplace(body, final)
    final

#Inside Component

template pins*(final: Component, body: untyped) =
    var internalPins: seq[tuple[n: uint, p: Pin]]
    template `->`(slice: HSlice[PinRef, PinRef], pin: Pin) =
        for i in slice.a.uint .. slice.b.uint:
            internalPins.add (n: i, p: pin)

    template `->`(p1: PinRef, pin: Pin) =
        internalPins.add (n: p1.uint, p: pin)

    body

    for p in internalPins:
        final.pins[p.n] = (pin: p.p, conns: @[])

template pinsLogic*(final: Component, body: untyped) =
    var logics = initTable[uint, proc(netIn: var Net, p1, p2: var tuple[n: uint, p: Pin])]()
    macro logicGenerator(code: untyped): untyped =
        result = nnkLambda.newTree(
                    newEmptyNode(),
                    newEmptyNode(),
                    newEmptyNode(),
                    nnkFormalParams.newTree(
                        newEmptyNode(),
                        nnkIdentDefs.newTree(
                            newIdentNode("netIn"),
                            nnkVarTy.newTree(
                                newIdentNode("Net")
                            ),
                            newEmptyNode()
                        ),
                        nnkIdentDefs.newTree(
                            newIdentNode("p1"),
                            newIdentNode("p2"),
                            nnkVarTy.newTree(nnkTupleTy.newTree(
                                nnkIdentDefs.newTree(newIdentNode("n"), newIdentNode("uint"), newEmptyNode()),
                                nnkIdentDefs.newTree(
                                    newIdentNode("p"),
                                    newIdentNode("Pin"),
                                    newEmptyNode()
                                )
                            )),
                            newEmptyNode()
                        )
                    ),
                    newEmptyNode(),
                    newEmptyNode(),
                nnkStmtList.newTree(code)
                )
    
    template logic(name: LogicRef, code: untyped): untyped = 
        logics[name.uint] = logicGenerator(code)

    template `->`(name: LogicRef, fun: proc(netIn: var Net, p1, p2: tuple[n: uint, p: var Pin])) =
        logics[name.uint] = fun
    
    template `<->`(p1, p2: PinRef) =
        final.pins[p1.uint].conns.add p2.uint
        final.pins[p2.uint].conns.add p1.uint
    
    template `>`(p1, p2: PinRef) =
        p1 <-> p2

    template `<`(p1: PinRef, logic: LogicRef, #[p2: uint]#): PinRef =
        final.logics[p1.uint] = logics[logic.uint]
        #final.logics[p2] = logics[logic]
        p1

    body

#Inside Circuit

template nets*(final: Circuit, body: untyped) =
    var nets = initTable[uint, Net]()
    template `->`(n: NetRef, nset: Net) =
        nets[n.uint] = nset
    
    body

    for net in nets.values:
        final.nets.add net

template components*(final: Circuit, body: untyped) =
    template `->`(u: tuple[r: string, n: uint], c: Component) =
        if not final.components.hasKey(u.r):
            final.components.add(u.r, initTable[uint, Component]())
        final.components[u.r][u.n] = c

    body
    #TODO
    #CR1 = comp:
    #    -> p1 (as input)
    #    <- p2 (as output)
    #    (Raise an Error if the Pin is not an IO)

template buses(final: Circuit, body: untyped) =
    #TODO
    discard

template connections*(final: Circuit, body: untyped) =
    #TODO optimization (use some graph library)
    var nets: seq[seq[CircuitPin]]

    var lastNet: uint
    var lastNets: array[2, uint]

    proc inNet(p: CircuitPin): int =
        for i in 0..nets.len()-1:
            for pin in nets[i]:
                if pin == p:
                    return i
        return -1

    # CU2.p2 <-> CU1.p1
    template `<->`(c1, c2: CircuitPin): CircuitPin =
        if c1.inNet() != -1 and c2.inNet() != -1:
            nets[c1.inNet()].add c2.inNet()
            nets[c2.inNet()].delete()
        elif c1.inNet() != -1:
            nets[c1.inNet()].add c2
        elif c2.inNet() != -1:
            nets[c2.inNet()].add c1
        else:
            nets.add @[c2, c1]
        c2

    template `<->`(c1: CircuitPin, c2: array[2, CircuitPin]): CircuitPin =
        discard c1 <-> c2[0]
        c2[1]
    
    template `<->`(c1: NetRef, c2: CircuitPin): uint =
        final.nets[c1.uint].add c2
        c1

    template `<->`(c1: CircuitPin, c2: NetRef): uint =
        final.nets[c2.uint].add c1
        c2

    proc addToNet(pin: CircuitPin, net: uint) =
        var net2 = pin.inNet()
        if net2 != -1:
            final.nets[net].pins.add nets[net2]
            nets[net2] = @[]
        else:
            final.nets[net].pins.add pin

    # | N35
    # | CU1.p1
    # | CU2.p2
    template `|`(net: NetRef) =
        lastNet = net.uint

    template `|`(pin: CircuitPin) =
        addToNet(pin, lastNet)

    #   N34 || N35
    # | p1.CU1.p2 |
    # | p1.CU2.p2 |
    template `||`(net1, net2: NetRef) =
        lastNets = [net1.uint, net2.uint]

    template `||`(pins: array[2, CircuitPin]) =
        addToNet(pins[0], lastNets[0])
        addToNet(pins[1], lastNets[1])

    body

    for net in nets:
        final.nets.add Net(pins: net)

when(isMainModule):
    import options, composer, generator

    let f = component("led"):
        pins:
            P1 -> PinIn((1.0,5.0), (0.1,0.3))
            P2..P3 -> PinOut()

        pinsLogic:
            logic L1:
                p2.p.maxVoltage = p1.p.maxVoltage

            P1 <-> P2
            P1<L1>P2

    echo f

    let c = cirucit("ledTest"): 
        nets:
            N0 -> Net(name: "VDD")
            N1 -> Net()

        components:
            CL0 -> f
            CL1 -> f

        connections:
            N0 || N1
            || P1.CL0.P2
            || P1.CL1.P2
    
    echo c
    echo c.compose(2).errors.len

    echo c.toFritzingNetlist("test")
    echo c.toKicadNetlist('D')


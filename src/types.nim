import options, tables, cascade


type
    PinsKinds* = enum Input, Output, Io
    CircuitErrorType* = enum IncorrectVoltage, IncorrectAmperage, UnsuitablePower, UnconnectedPin

    Pin* = object
        minVoltage*: float
        maxVoltage*: float
        minCurrent*: float
        maxCurrent*: float
        case kind*: PinsKinds
            of Io: normalMode*: PinsKinds
            of Output: needforward*: bool
            else: discard

    Component* = object
        name*: string
        libsource*: string #TODO: importing kicad libs
        pins*: Table[uint, tuple[pin: Pin, conns: seq[uint]]]
        logics*: Table[uint, proc(netIn: var Net, p1, p2: var tuple[n: uint, p: Pin])]

    ComponentPin* = tuple[r: string, n: uint]
    CircuitPin* = tuple[c: ComponentPin, p: uint]


    Net* = object
        name*: string
        pins*: seq[CircuitPin]
        minVoltage: Option[float]
        maxVoltage: Option[float]
        minCurrent: Option[float]
        maxCurrent: Option[float]

    Bus* = object
        pinsToPin*: Option[tuple[pin: CircuitPin, pins: seq[CircuitPin]]]
        pinsToPins*: Option[Table[CircuitPin, CircuitPin]]


    Circuit* = object
        components*: Table[string, Table[uint, Component]]
        buses*: seq[Bus]
        nets*: seq[Net]
        ignore*: seq[CircuitPin]

    CircuitError* = ref object of Exception
        kind*: CircuitErrorType 
        pin*: CircuitPin
        net*: Option[Net]
        bus*: Option[Bus]


proc getBusPins*(b: Bus): seq[CircuitPin] =
    if b.pinsToPin.isSome():
        let pins = b.pinsToPin.get
        result[0] = pins.pin
        result.add pins.pins
    else:
        let pins = b.pinsToPins.get
        for p in pins.keys:
            result.add p
        for p in pins.values:
            result.add p  


proc isNetAssigned*(net: Net): bool =
    if net.minVoltage.isSome or net.minCurrent.isSome:
        return false
    return true

proc private_assign_net_data*(net: Net, minVoltage, maxVoltage, minCurrent, maxCurrent: float): Net =
    result = cascade net:
        minVoltage = minVoltage.some
        maxVoltage = maxVoltage.some
        minCurrent = minCurrent.some
        maxCurrent = maxCurrent.some
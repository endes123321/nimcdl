import types, xmltree, times, strtabs, tables, cascade

proc generateFritzingParts(circuit: Circuit): Table[ComponentPin, XmlNode] =
    result = initTable[ComponentPin, XmlNode]()

    for samecomponents in circuit.components.keys:
        for i in 0..circuit.components[samecomponents].len()-1:
            result[(r: samecomponents, n: i.uint)] = <>part(label=samecomponents & $i, id= $samecomponents[0].uint & $i, title="TODO") #TODO

proc generateKicadParts(circuit: Circuit): seq[XmlNode] =
    for samecomponents in circuit.components.keys:
        for i in 0..circuit.components[samecomponents].len()-1:
            let comp_args = newStringTable()
            comp_args["ref"]= samecomponents & $i
            let data = circuit.components[samecomponents][i.uint]
            let comp = cascade newElement("comp"):
                attrs = comp_args
                add <>value(newText(data.name))
                add <>libsource(lib=data.libsource, part=data.name)
                add <>sheetpath(names="/", tstamps="/")
                #add <>tstamp(newText("TODO"))
            result.add comp

proc generateFritzingNets(circuit: Circuit, elements: Table[ComponentPin, XmlNode]): seq[XmlNode] =
    for net in circuit.nets:
        var finalNet = <>net()
        for pin in net.pins:
            var connector = <>connector(name= "Pin " & $pin.p, id= "connector" & $pin.p)
            connector.add elements[pin.c]

            finalNet.add connector
        result.add finalNet

proc generateKicadNets(circuit: Circuit): seq[XmlNode] =
    for inet in 0..circuit.nets.len()-1:
        var net = <>net(code= $inet, name=circuit.nets[inet].name)
        for pin in circuit.nets[inet].pins:
            let node_args = newStringTable()
            node_args["ref"]= pin.c.r & $pin.c.n
            node_args["pin"]= $pin.p
            let node = newElement("node")
            node.attrs = node_args
            net.add node
        result.add net


proc toFritzingNetlist*(circuit: Circuit, name: string): string =
    result = xmlHeader
    result &= "<!-- Created with nimcdl 0.1.0 by endes -->\n"
    var netlist = <>netlist(sketch=name, date=format(now(), "ddd MMM dd HH:mm:ss yyyy"))
    for net in circuit.generateFritzingNets(circuit.generateFritzingParts()):
        netlist.add net
    result &= netlist

proc toKicadNetlist*(circuit: Circuit, version: char): string =
    result = xmlHeader
    let header = cascade <>design():
        add <>source(newText("TODO")) #TODO: file source
        add <>date(newText(format(now(), "dd/mm/yyyy HH:mm:ss")))
        add <>tool(newText("nimcdl 0.1.0 by endes"))

    var components = <>components()
    for comp in circuit.generateKicadParts():
        components.add comp

    var netlist = <>nets()
    for net in circuit.generateKicadNets():
        netlist.add net

    let final_args = newStringTable()
    final_args["version"]= $version
    let final = cascade newElement("export"):
        attrs = final_args
        add header
        add components
        add netlist

    result &= final

# Package

version     = "0.1.0"
author      = "endes"
description = "Create circuits and printed circuit boards with nim!"
license     = "GPL"
srcDir      = "src"

# Deps

requires "cascade >= 0.2.0"
requires "nim >= 0.19.0"